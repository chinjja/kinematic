package com.chinjja.kinematic;

//import static java.lang.Math.*;
import static org.apache.commons.math3.util.FastMath.*;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Utils {
	static double[] a(double...array) {
		return array;
	}
	
	static double[][] data(double...array) {
		int dim = (int)round(sqrt(array.length));
		double[][] d = new double[dim][dim];
		for(int r = 0; r < dim; r++) {
			for(int c = 0; c < dim; c++) {
				d[r][c] = array[r*dim+c];
			}
		}
		return d;
	}
	
	static RealMatrix create(double...data) {
		return MatrixUtils.createRealMatrix(data(data));
	}
	
	static RealMatrix createRow(RealVector vector) {
		return createRow(vector.toArray());
	}
	
	static RealMatrix createRow(double...data) {
		return MatrixUtils.createRowRealMatrix(data);
	}
	
	static RealMatrix createColumn(RealVector vector) {
		return createColumn(vector.toArray());
	}
	
	static RealMatrix createColumn(double...data) {
		return MatrixUtils.createColumnRealMatrix(data);
	}
	
	static RealMatrix translate(double x, double y, double z) {
		return create(
					1, 0, 0, x,
					0, 1, 0, y,
					0, 0, 1, z,
					0, 0, 0, 1);
	}
	
	// rz
	static RealMatrix roll(double t) {
		return roll(t, Vector3D.ZERO);
	}
	
	static RealMatrix roll(double t, Vector3D vector) {
		t = toRadians(t);
		return create(
					cos(t), -sin(t), 0, vector.getX(),
					sin(t),  cos(t), 0, vector.getY(),
					0,       0,      1, vector.getZ(),
					0,       0,      0, 1);
	}
	
	// ry
	static RealMatrix pitch(double t) {
		return pitch(t, Vector3D.ZERO);
	}
	
	static RealMatrix pitch(double t, Vector3D vector) {
		t = toRadians(t);
		return create(
					 cos(t), 0, sin(t), vector.getX(),
					 0,      1, 0,      vector.getY(),
					-sin(t), 0, cos(t), vector.getZ(),
					 0,      0, 0,      1);
	}
	
	// rx
	static RealMatrix yaw(double t) {
		return yaw(t, Vector3D.ZERO);
	}
	
	static RealMatrix yaw(double t, Vector3D vector) {
		t = toRadians(t);
		return create(
					1, 0,       0,      vector.getX(),
					0, cos(t), -sin(t), vector.getY(),
					0, sin(t),  cos(t), vector.getZ(),
					0, 0,       0,      1);
	}
	
	static RealMatrix rpy(double r, double p, double y) {
		return roll(r).multiply(pitch(p)).multiply(yaw(y));
	}
	
	static double[] translate(RealMatrix m) {
		return new double[] {
				m.getEntry(0, 3),
				m.getEntry(1, 3),
				m.getEntry(2, 3),
		};
	}
	
	// rz
	static double roll(RealMatrix m) {
		return toDegrees(atan(m.getEntry(1, 0) / m.getEntry(0, 0)));
	}
	
	// ry
	static double pitch(RealMatrix m) {
//		return toDegrees(asin(m.getEntry(2, 0)));
		return toDegrees(atan(-m.getEntry(2, 0) / sqrt(pow(m.getEntry(2, 1), 2) + pow(m.getEntry(2, 2), 2))));
	}
	
	// rx
	static double yaw(RealMatrix m) {
		return toDegrees(atan(m.getEntry(2, 1) / m.getEntry(2, 2)));
	}
	
	// euler
	static double[] rpy(RealMatrix m) {
		return a(roll(m), pitch(m), yaw(m));
	}
	
	static double[] pose(RealMatrix m) {
		double[] xyz = translate(m);
		double[] rpy = rpy(m);
		return new double[] {
				xyz[0], xyz[1], xyz[2], rpy[0], rpy[1], rpy[2]
		};
	}
}