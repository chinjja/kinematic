package com.chinjja.kinematic;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.RealMatrix;

public class Joint {
	public final JointType type;
	public final Vector3D position;
	private double angle;
	
	public Joint(JointType type) {
		this(type, Vector3D.ZERO);
	}
	
	public Joint(JointType type, Vector3D position) {
		this.type = type;
		this.position = position;
	}
	
	public RealMatrix getMatrix(double angle) {
		switch(type) {
		case X:	return Utils.yaw(angle, position);
		case Y:	return Utils.pitch(angle, position);
		case Z:	return Utils.roll(angle, position);
		default:throw new IllegalArgumentException();
		}
	}
	
	public RealMatrix getMatrix() {
		return getMatrix(angle);
	}
	
	public void setAngle(double angle) {
		this.angle = angle;
	}
	
	public double getAngle() {
		return angle;
	}
}

