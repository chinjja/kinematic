package com.chinjja.kinematic;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealVector;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.GridLayout;

public class KinematicUI extends JFrame {
	private Kinematic k = createKinematic();
	private boolean ik;
	private boolean fk;
	
	private final JLabel lblNewLabel = new JLabel("Joint");
	private final JLabel lblNewLabel_1 = new JLabel("Pose");
	private final JSpinner j0 = new JSpinner();
	private final JSpinner j1 = new JSpinner();
	private final JSpinner j2 = new JSpinner();
	private final JSpinner j3 = new JSpinner();
	private final JSpinner j4 = new JSpinner();
	private final JSpinner j5 = new JSpinner();
	private final JSpinner p0 = new JSpinner();
	private final JSpinner p1 = new JSpinner();
	private final JSpinner p2 = new JSpinner();
	private final JSpinner p3 = new JSpinner();
	private final JSpinner p4 = new JSpinner();
	private final JSpinner p5 = new JSpinner();
	private final JButton btnNewButton = new JButton("0.00001");
	private final Preview preview1 = new Preview(k, MatrixUtils.createRealIdentityMatrix(4));
	private final Preview preview2 = new Preview(k, Utils.yaw(90));
	private final JPanel panel = new JPanel();
	public KinematicUI() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{80, 80, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);
		
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.gridx = 1;
		gbc_lblNewLabel_1.gridy = 0;
		getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		GridBagConstraints gbc_j0 = new GridBagConstraints();
		gbc_j0.fill = GridBagConstraints.HORIZONTAL;
		gbc_j0.gridx = 0;
		gbc_j0.gridy = 1;
		j0.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fk();
			}
		});
		j0.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		getContentPane().add(j0, gbc_j0);
		
		GridBagConstraints gbc_p0 = new GridBagConstraints();
		gbc_p0.fill = GridBagConstraints.HORIZONTAL;
		gbc_p0.gridx = 1;
		gbc_p0.gridy = 1;
		p0.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ik();
			}
		});
		p0.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(0.1)));
		getContentPane().add(p0, gbc_p0);
		
		GridBagConstraints gbc_j1 = new GridBagConstraints();
		gbc_j1.fill = GridBagConstraints.HORIZONTAL;
		gbc_j1.gridx = 0;
		gbc_j1.gridy = 2;
		j1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fk();
			}
		});
		
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 8;
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 2;
		gbc_panel.gridy = 1;
		getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		panel.add(preview1);
		preview1.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(preview2);
		preview2.setBorder(new LineBorder(new Color(0, 0, 0)));
		j1.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		getContentPane().add(j1, gbc_j1);
		
		GridBagConstraints gbc_p1 = new GridBagConstraints();
		gbc_p1.fill = GridBagConstraints.HORIZONTAL;
		gbc_p1.gridx = 1;
		gbc_p1.gridy = 2;
		p1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ik();
			}
		});
		p1.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(0.1)));
		getContentPane().add(p1, gbc_p1);
		
		GridBagConstraints gbc_j2 = new GridBagConstraints();
		gbc_j2.fill = GridBagConstraints.HORIZONTAL;
		gbc_j2.gridx = 0;
		gbc_j2.gridy = 3;
		j2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fk();
			}
		});
		j2.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		getContentPane().add(j2, gbc_j2);
		
		GridBagConstraints gbc_p2 = new GridBagConstraints();
		gbc_p2.fill = GridBagConstraints.HORIZONTAL;
		gbc_p2.gridx = 1;
		gbc_p2.gridy = 3;
		p2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ik();
			}
		});
		p2.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(0.1)));
		getContentPane().add(p2, gbc_p2);
		
		GridBagConstraints gbc_j3 = new GridBagConstraints();
		gbc_j3.fill = GridBagConstraints.HORIZONTAL;
		gbc_j3.gridx = 0;
		gbc_j3.gridy = 4;
		j3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fk();
			}
		});
		j3.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		getContentPane().add(j3, gbc_j3);
		
		GridBagConstraints gbc_p3 = new GridBagConstraints();
		gbc_p3.fill = GridBagConstraints.HORIZONTAL;
		gbc_p3.gridx = 1;
		gbc_p3.gridy = 4;
		p3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ik();
			}
		});
		p3.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(0.5)));
		getContentPane().add(p3, gbc_p3);
		
		GridBagConstraints gbc_j4 = new GridBagConstraints();
		gbc_j4.fill = GridBagConstraints.HORIZONTAL;
		gbc_j4.gridx = 0;
		gbc_j4.gridy = 5;
		j4.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fk();
			}
		});
		j4.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		getContentPane().add(j4, gbc_j4);
		
		GridBagConstraints gbc_p4 = new GridBagConstraints();
		gbc_p4.fill = GridBagConstraints.HORIZONTAL;
		gbc_p4.gridx = 1;
		gbc_p4.gridy = 5;
		p4.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ik();
			}
		});
		p4.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(0.5)));
		getContentPane().add(p4, gbc_p4);
		
		GridBagConstraints gbc_j5 = new GridBagConstraints();
		gbc_j5.fill = GridBagConstraints.HORIZONTAL;
		gbc_j5.gridx = 0;
		gbc_j5.gridy = 6;
		j5.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				fk();
			}
		});
		j5.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1)));
		getContentPane().add(j5, gbc_j5);
		
		GridBagConstraints gbc_p5 = new GridBagConstraints();
		gbc_p5.fill = GridBagConstraints.HORIZONTAL;
		gbc_p5.gridx = 1;
		gbc_p5.gridy = 6;
		p5.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ik();
			}
		});
		p5.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(0.5)));
		getContentPane().add(p5, gbc_p5);
		
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 7;
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RealVector v = MatrixUtils.createRealVector(new double[] {
						0.00001, -0.00001, 0.00001, -0.00001, -0.00001, 0.00001
				});
				fromVector(v, j0, j1, j2, j3, j4, j5);
			}
		});
		getContentPane().add(btnNewButton, gbc_btnNewButton);
		
		fk();
	}
	
	private void fk() {
		if(ik) return;
		fk = true;
		k.setJointAngles(toVector(j0, j1, j2, j3, j4, j5));
		RealVector v = k.forward();
		fromVector(v, p0, p1, p2, p3, p4, p5);
		fk = false;
		preview1.repaint();
		preview2.repaint();
	}
	
	private void ik() {
		if(fk) return;
		ik = true;
		k.setJointAngles(toVector(j0, j1, j2, j3, j4, j5));
		RealVector v = k.inverse(toVector(p0, p1, p2, p3, p4, p5));
		RealVector v2 = MatrixUtils.createRealVector(new double[] {
				v.getEntry(0) % 360,
				v.getEntry(1) % 360,
				v.getEntry(2) % 360,
				v.getEntry(3) % 360,
				v.getEntry(4) % 360,
				v.getEntry(5) % 360,
		});
		fromVector(v2, j0, j1, j2, j3, j4, j5);
		ik = false;
		preview1.repaint();
		preview2.repaint();
	}
	
	private Kinematic createKinematic() {
		Kinematic k = new Kinematic();
		k.addJoint(new Joint(JointType.Z));
		k.addJoint(new Joint(JointType.Y, new Vector3D(3, 0, 3)));
		k.addJoint(new Joint(JointType.Y, new Vector3D(-7, 0, 7)));
		k.addJoint(new Joint(JointType.Z, new Vector3D(7, 0, 1)));
		k.addJoint(new Joint(JointType.Y));
		k.addJoint(new Joint(JointType.Z, new Vector3D(2, 0, 0)));
		return k;
	}

	private RealVector toVector(JSpinner...spinners) {
		double[] data = new double[spinners.length];
		for(int i = 0; i < data.length; i++) {
			data[i] = (Double)spinners[i].getValue();
		}
		return MatrixUtils.createRealVector(data);
	}
	
	private void fromVector(RealVector v, JSpinner...spinners) {
		for(int i = 0; i < v.getDimension(); i++) {
			spinners[i].setValue(v.getEntry(i));
		}
	}
	
	public static void main(String[] args) throws UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(new NimbusLookAndFeel());
		KinematicUI ui = new KinematicUI();
		ui.setSize(600, 800);
		ui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ui.setLocationRelativeTo(null);
		ui.setVisible(true);
	}
}
