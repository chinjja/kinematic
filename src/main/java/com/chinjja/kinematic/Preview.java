package com.chinjja.kinematic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import javax.swing.JComponent;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Preview extends JComponent {
	Kinematic k;
	RealMatrix world;
	Preview(Kinematic k, RealMatrix world) {
		this.k = k;
		this.world = world;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		double w = getWidth();
		double h = getHeight();
		
		RealMatrix m = world;
		Color[] color = {
				Color.RED,
				Color.BLUE,
				Color.GREEN,
				Color.YELLOW,
				Color.ORANGE,
				Color.BLACK
		};
		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		Point2D prev = null;
		for(int i = 0; i < k.getJointCount(); i++) {
			Joint j = k.getJoint(i);
			RealMatrix in = MatrixUtils.createColumnRealMatrix(new double[] {
					j.position.getX(),
					j.position.getY(),
					j.position.getZ(),
					1
			});
			RealMatrix p = m.multiply(in);
			double x = w/2 + p.getEntry(0, 0)*10;
			double y = h/2 + p.getEntry(1, 0)*10;
			if(prev != null) {
				g2.draw(new Line2D.Double(prev.getX(), prev.getY(), x, y));
			}
			prev = new Point2D.Double(x, y);
			Ellipse2D.Double ellipse = new Ellipse2D.Double(x-5, y-5, 10, 10);
			g2.setPaint(color[i]);
			g2.drawString(""+i, (float)(x+5), (float)(y-5));
			g2.fill(ellipse);
			m = m.multiply(j.getMatrix());
		}
	}

	RealMatrix glOrtho(double l, double r, double b, double t, double n, double f) {
		double[][] M = new double[4][4];
		// set OpenGL perspective projection matrix
		M[0][0] = 2 / (r - l);
		M[0][1] = 0;
		M[0][2] = 0;
		M[0][3] = 0;

		M[1][0] = 0;
		M[1][1] = 2 / (t - b);
		M[1][2] = 0;
		M[1][3] = 0;

		M[2][0] = 0;
		M[2][1] = 0;
		M[2][2] = -2 / (f - n);
		M[2][3] = 0;

		M[3][0] = -(r + l) / (r - l);
		M[3][1] = -(t + b) / (t - b);
		M[3][2] = -(f + n) / (f - n);
		M[3][3] = 1;
		return MatrixUtils.createRealMatrix(M);
	}

	RealMatrix ortho(double left, double right, double bottom, double top, double near, double far) {
		RealMatrix T = translate(-(left + right) / 2, -(top + bottom) / 2, near / 2);
		RealMatrix S = scale(2 / (left - right), 2 / (top - bottom), 1
				/ (far - near));
		RealMatrix V = S.multiply(T);
		return V;

	}
	
	RealMatrix scale(double sx, double sy, double sz) {
		return Utils.create(
				sx, 0, 0, 0,
				0, sy, 0, 0,
				0, 0, sz, 0,
				0, 0, 0, 1
				);
	}
	
	RealMatrix translate(double x, double y, double z) {
		return Utils.create(
				1, 0, 0, x,
				0, 1, 0, y,
				0, 0, 0, z,
				0, 0, 0, 1
				);
	}
}
