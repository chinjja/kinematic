package com.chinjja.kinematic;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Kinematic {
	static int ITER = 1000;
	static double EPS = 1e-7;
	
	private final List<Joint> links = new ArrayList<>();
	
	public void addJoint(Joint joint) {
		links.add(joint);
	}
	
	public Joint getJoint(int i) {
		return links.get(i);
	}
	
	public int getJointCount() {
		return links.size();
	}
	
	public void setJointAngles(RealVector angles) {
		for(int i = 0; i < getJointCount(); i++) {
			getJoint(i).setAngle(angles.getEntry(i));
		}
	}
	
	public RealVector getJointAngles() {
		double[] data = new double[getJointCount()];
		for(int i = 0; i < getJointCount(); i++) {
			data[i] = getJoint(i).getAngle();
		}
		return MatrixUtils.createRealVector(data);
	}
	
	public RealVector forward() {
		return forward(getJointAngles());
	}
	
	public RealVector forward(RealVector angles) {
		RealMatrix m = MatrixUtils.createRealIdentityMatrix(4);
		for(int i = 0; i < 6; i++) {
			m = m.multiply(links.get(i).getMatrix(angles.getEntry(i)));
		}
		double[] pose = Utils.pose(m);
		return MatrixUtils.createRealVector(pose);
	}
	
	public RealVector inverse(RealVector P) {
		return inverse(getJointAngles(), P);
	}
	
	public RealVector inverse(RealVector A, RealVector P) {
		int i = 0;
		try {
		while(i++ < ITER) {
			RealVector O = forward(A);
			RealMatrix J = J(A, 0.01);
			RealMatrix D = MatrixUtils.createColumnRealMatrix(P.subtract(O).toArray());
			RealVector R = MatrixUtils.inverse(J).multiply(D).getColumnVector(0);
			A = A.add(R);
			if(P.getDistance(forward(A)) < EPS) {
				break;
			}
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("iteration="+i + ", distance="+P.getDistance(forward(A)));
		return A;
	}
	
	// f(x+dx)-f(x)/d0
	RealMatrix J(RealVector a, double d) {
		final RealVector from = forward(a);
		RealVector to;
		RealVector delta;
		
		RealMatrix J = MatrixUtils.createRealMatrix(6, 6);
		for(int i = 0; i < 6; i++) {
			double[] d0 = new double[6];
			d0[i] = d;
			to = forward(a.add(MatrixUtils.createRealVector(d0)));
			delta = to.subtract(from).mapDivide(d);
			J.setColumnVector(i, delta);
		}
		
		return J;
	}
	
	public static void main(String[] args) {
		Kinematic k = new Kinematic();
		k.addJoint(new Joint(JointType.Z));
		k.addJoint(new Joint(JointType.Y, new Vector3D(3, 0, 3)));
		k.addJoint(new Joint(JointType.Y, new Vector3D(0, 0, 7)));
		k.addJoint(new Joint(JointType.Z, new Vector3D(0, 0, 8)));
		k.addJoint(new Joint(JointType.Y));
		k.addJoint(new Joint(JointType.Z, new Vector3D(0, 0, 2)));
		
		RealVector angles = MatrixUtils.createRealVector(new double[] { 0, 45, 45, 0, 60, 0 });
		System.out.println("FK: " + k.forward(angles));
		System.out.println(angles);
		RealVector IK = k.inverse(angles, MatrixUtils.createRealVector(new double[] {
				7.7781745931, 7.7781745931, 8, 35, 0, -0
		}));
		System.out.println("IK: " + IK);
		System.out.println(k.forward(IK));
	}
}
